/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;


public class Chauffeur {
    
    private int id;
    private Voiture voiture;
    private String nom;
    private int age;
    private Agence agence;
    private float prix;

    public Chauffeur() {
    }

    public Chauffeur(int id, Voiture voiture, String nom, int age, Agence agence, float prix) {
        this.id = id;
        this.voiture = voiture;
        this.nom = nom;
        this.age = age;
        this.agence = agence;
        this.prix = prix;
    }

    public Chauffeur(Voiture voiture, String nom, int age, Agence agence, float prix) {
        this.voiture = voiture;
        this.nom = nom;
        this.age = age;
        this.agence = agence;
        this.prix = prix;
    }

    public Chauffeur(String nom, int age, Agence agence, float prix) {
        this.nom = nom;
        this.age = age;
        this.agence = agence;
        this.prix = prix;
    }

    

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Voiture getVoiture() {
        return voiture;
    }

    public void setVoiture(Voiture voiture) {
        this.voiture = voiture;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Agence getAgence() {
        return agence;
    }

    public void setAgence(Agence agence) {
        this.agence = agence;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        return nom ;
    }
    
}
