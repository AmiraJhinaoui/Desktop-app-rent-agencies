/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;
import dao.DaoAgence;
import dao.DaoChauffeur;
import dao.DaoLocation;
import entity.Agence;
import entity.Chauffeur;
import entity.Location;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import utils.Session;

public class FXMLListeDesDemandesController implements Initializable {

    @FXML
    private TableView Liste_DemandeLocation;
    @FXML
    private TableColumn column_Client;
    @FXML
    private TableColumn column_Agence;
    @FXML
    private TableColumn column_Matricule;
    @FXML
    private TableColumn column_DateDebut;
    @FXML
    private TableColumn column_DateFin;
    @FXML
    private TableColumn column_Chauffeur;
    @FXML
    private ChoiceBox choix_chauffeur;
    @FXML
    private Button button_approuverDemandeLocation;
     
    @FXML
    private AnchorPane Liste_demandeLocation;
    @FXML
    private Button btn_choisir_location;
    @FXML
    private Label label_location_choisi;
    
    public Location locationChosen = null;
    
    
    
   /* public void showAction(MouseEvent ev){
        if(ev.getButton().equals(MouseButton.PRIMARY)){
            if(ev.getClickCount() == 2){
                System.out.println("Double clicked");
            }
        }
        Location Loc = (Location) Liste_DemandeLocation.getSelectionModel().getSelectedItem();
        System.out.println(Loc);
    }*/
    
    public void resetTable()
    {
        List<Location> listLocation = new ArrayList<>();
        DaoLocation daoLoc = DaoLocation.getInstance();
        System.out.println(Session.getLoggedInUser().getId());
        listLocation = daoLoc.getAllLocationNonApprouvedByManager(Session.getLoggedInUser().getId());
         System.out.println(listLocation);
        ObservableList<Location> data = FXCollections.observableArrayList(listLocation);
        Liste_DemandeLocation.setItems(data);
    }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        List<Location> listLocation = new ArrayList<>();
        DaoLocation daoLoc = DaoLocation.getInstance();
        System.out.println(Session.getLoggedInUser().getId());
        listLocation = daoLoc.getAllLocationNonApprouvedByManager(Session.getLoggedInUser().getId());
         System.out.println(listLocation);
        ObservableList<Location> data = FXCollections.observableArrayList(listLocation);
        
        column_Client.setCellValueFactory(
            new PropertyValueFactory<Location,String>("client")
        );
        column_Agence.setCellValueFactory(
            new PropertyValueFactory<Location,String>("agence")
        );
        column_Matricule.setCellValueFactory(
            new PropertyValueFactory<Location,String>("voiture")
        );
        column_DateDebut.setCellValueFactory(
            new PropertyValueFactory<Location,String>("dateDebut")
        );
        column_DateFin.setCellValueFactory(
            new PropertyValueFactory<Location,String>("dateFin")
        );
        column_Chauffeur.setCellValueFactory(
            new PropertyValueFactory<Location,Boolean>("avecChauffeur")
        );
       
        
        Liste_DemandeLocation.setItems(data);
    }    

    
        
       

    @FXML
    private void ApprouverDemandeLocation(ActionEvent event) {
        //String nomCh = (String) choix_chauffeur.getSelectionModel().getSelectedItem().toString();
        //Chauffeur c = DaoChauffeur.getInstance().findByNom(nomCh, locationChosen.getAgence().getId_agence());
        if(locationChosen == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Choisir une location ");
            alert.showAndWait();
        }else{
            if(DaoLocation.getInstance().approuverLocation(locationChosen)){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Succée");
            alert.setContentText("location approuvé");
            alert.showAndWait();
            resetTable();
            }
        }
        
    }

    @FXML
    private void AffecterUnChauffeur(MouseEvent event) {
         //choix_chauffeur.setItems(FXCollections.observableArrayList(DaoChauffeur.getInstance().getAllByManager(Session.getLoggedInUser()));
    }

    @FXML
    private void choisirLocation(ActionEvent event) {
        Location loc = (Location) Liste_DemandeLocation.getSelectionModel().getSelectedItem();
        locationChosen = loc;
        if(loc == null){
            
        }else{
            label_location_choisi.setText("Agence : " + loc.getAgence().getNom_agence());
            List<Chauffeur> listCh = new ArrayList<>();
            listCh = DaoChauffeur.getInstance().getAllByAgence(loc.getAgence().getId_agence());
            choix_chauffeur.setItems(FXCollections.observableArrayList(listCh));
        }
    }

    @FXML
    private void logOut(MouseEvent event) {
        Session.setLoggedInUser(null);
        Parent root;
             try {
                 root = FXMLLoader.load(getClass().getResource("/gui/FXMLLogin.fxml"));
                 Stage myWindow = (Stage) Liste_DemandeLocation.getScene().getWindow();
                 Scene sc = new Scene(root);
                 myWindow.setScene(sc);
                 myWindow.setTitle("Login");
                 myWindow.show();
             } catch (IOException ex) {
                 Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
             }
    }
}