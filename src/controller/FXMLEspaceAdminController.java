/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import utils.Session;


public class FXMLEspaceAdminController implements Initializable {

    @FXML
    private AnchorPane pane_agences;
    @FXML
    private AnchorPane pane_membres_karhabty;
    @FXML
    private AnchorPane pane_demande_membership;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        try {
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/FXMLDemandeDeMembership.fxml"));
            pane_demande_membership.getChildren().setAll(pane);
        } catch (IOException ex) {
            Logger.getLogger(FXMLAjouterAgenceLocation1Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/FXMLListeAgenceLocation.fxml"));
            pane_agences.getChildren().setAll(pane);
        } catch (IOException ex) {
            Logger.getLogger(FXMLAjouterAgenceLocation1Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/FXMLListeMembreKarhabty.fxml"));
            pane_membres_karhabty.getChildren().setAll(pane);
        } catch (IOException ex) {
            Logger.getLogger(FXMLAjouterAgenceLocation1Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void logOut(MouseEvent event) {
        Session.setLoggedInUser(null);
        Parent root;
             try {
                 root = FXMLLoader.load(getClass().getResource("/gui/FXMLLogin.fxml"));
                 Stage myWindow = (Stage) pane_agences.getScene().getWindow();
                 Scene sc = new Scene(root);
                 myWindow.setScene(sc);
                 myWindow.setTitle("Login");
                 myWindow.show();
             } catch (IOException ex) {
                 Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
             }
    }
    
}
