/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DaoAgence;
import dao.DaoUser;
import entity.Agence;
import entity.User;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.util.converter.IntegerStringConverter;
import utils.FileUpload;

/**
 * FXML Controller class
 *
 * @author 3D-Artist
 */
public class FXMLAjouterAgenceLocation3Controller implements Initializable {

    File file = null;
    File filePiece = null;
    
    @FXML
    private ImageView img_agence;
    @FXML
    private TextField input_nom;
    @FXML
    private TextField input_tel;
    @FXML
    private TextField input_horaire;
    @FXML
    private TextField input_rue;
    @FXML
    private TextField input_ville;
    @FXML
    private TextField input_code_postal;
    @FXML
    private Label error_label;
    
    public static Agence agenceIsSet = null;
    public static File imgAgence = null;
    public static File imgPiece = null;
    @FXML
    private ImageView img_piece;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //FILTER TEXTFIELD TO ENTER ONLY NUMBERS
        UnaryOperator<TextFormatter.Change> integerFilter = change -> {
            String newText = change.getControlNewText();
            if (newText.matches("-?([1-9][0-9]*)?")) { 
                return change;
            }
            return null;
        };
        input_code_postal.setTextFormatter(new TextFormatter<Integer>(new IntegerStringConverter(), 0, integerFilter));
        input_tel.setTextFormatter(new TextFormatter<Integer>(new IntegerStringConverter(), 0, integerFilter));
        
        
        if(agenceIsSet != null){
            input_nom.setText(agenceIsSet.getNom_agence());
            input_tel.setText(Integer.toString(agenceIsSet.getTelephone_agence()));
            input_horaire.setText(agenceIsSet.getHoraire_travail());
            input_rue.setText(agenceIsSet.getRue());
            input_ville.setText(agenceIsSet.getVille());
            input_code_postal.setText(Integer.toString(agenceIsSet.getCode_postal()));
            try {
                URL urlImageAgence = imgAgence.toURI().toURL();
                img_agence.setImage(new Image(urlImageAgence.toExternalForm()));
                file = imgAgence;
                
                URL urlPieceAgence = imgAgence.toURI().toURL();
                img_piece.setImage(new Image(urlPieceAgence.toExternalForm()));
                filePiece = imgPiece;
            } catch (MalformedURLException ex) {
                Logger.getLogger(FXMLAjouterAgenceLocation3Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        System.out.println("heeeeeeeeeeeeeeeeeeeeeeeeeee");
        System.out.println(FXMLAjouterAgenceLocation2Controller.imgUser);
    }    

    @FXML
    private void uploadImage(ActionEvent event) throws MalformedURLException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        FileChooser.ExtensionFilter ext1 = new FileChooser.ExtensionFilter("JPG files(*.jpg)","*.JPG");
        FileChooser.ExtensionFilter ext2 = new FileChooser.ExtensionFilter("PNG files(*.png)","*.PNG");
        fileChooser.getExtensionFilters().addAll(ext1,ext2);
        file = fileChooser.showOpenDialog(img_agence.getScene().getWindow());
        if(file != null){
            URL url = file.toURI().toURL();
            img_agence.setImage(new Image(url.toExternalForm()));
            System.out.println(url.toExternalForm());
        }
    }

    @FXML
    private void ajouterAgence(ActionEvent event) {
        if(input_nom.getText().equals("") || input_tel.getText().equals("") || input_horaire.getText().equals("") || input_rue.getText().equals("") || input_ville.getText().equals("") || input_code_postal.getText().equals("") || input_tel.getText().equals("0") || input_code_postal.getText().equals("0")){
            error_label.setText("Remplissez tous les champs");
        }else{
            error_label.setText("");
            if(file == null){
                error_label.setText("mets une image");
                return;
            }else{
                error_label.setText("");
            }
            if(filePiece == null){
                error_label.setText("telecharger piece");
                return;
            }else{
                error_label.setText("");
            }
            Agence agence = new Agence();
            agence.setNom_agence(input_nom.getText());
            agence.setTelephone_agence(Integer.parseInt(input_tel.getText()));
            agence.setType_agence("LOCATION");
            agence.setHoraire_travail(input_horaire.getText());
            agence.setRue(input_rue.getText());
            agence.setCode_postal(Integer.parseInt(input_code_postal.getText()));
            agence.setVille(input_ville.getText());
            agence.setLatitude(0);
            agence.setLongitude(0);
            agence.setApprouved(false);
            agenceIsSet = agence;
            imgAgence = file;
            imgPiece = filePiece;
            
            ajoutFinale(FXMLAjouterAgenceLocation2Controller.userIsSet,agenceIsSet);
            
        }
    }
    
    public void ajoutFinale(User user,Agence agence){
        if(user == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error");
            alert.setContentText(" profile complet");
            alert.showAndWait();
            return;
        }
        if(agence == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error");
            alert.setContentText(" agence complete");
            alert.showAndWait();
            return;
        }
        if(user.getImage().equals("default")){
            
        }else{
            // UPLOAD THE IMAGE HERE
            
            FileUpload fileUpload = new FileUpload () ;
            String response = fileUpload.executeMultiPartRequest("http://localhost/karhabty/profile_imgs/upload.php", FXMLAjouterAgenceLocation2Controller.imgUser, user.getPrenom(), "amira") ;
            System.out.println("Response : "+response);
            if(response.equals("true")){
                System.out.println("telechargement complet");
            }else{
                System.out.println("no upload");
            }
            user.setImage("http://localhost/karhabty/profile_imgs/uploads/" + user.getPrenom());
        }
        boolean res = DaoUser.getInstance().registerClient(user);
        if(res){
            agence.setOwner(user);
            // UPLOAD THE IMAGE HERE
            FileUpload fileUpload = new FileUpload () ;
            String response = fileUpload.executeMultiPartRequest("http://localhost/karhabty/profile_imgs/upload.php", imgPiece, agence.getVille(), "amira") ;
            System.out.println("Response : "+response);
            if(response.equals("true")){
                System.out.println("telechargement complet");
            }else{
                System.out.println("pas de telechargement");
            }
            agence.setPiece_justificatif("http://localhost/karhabty/profile_imgs/uploads/" + agence.getVille());
            // UPLOAD THE IMAGE HERE
            FileUpload fileUpload2 = new FileUpload () ;
            String response2 = fileUpload2.executeMultiPartRequest("http://localhost/karhabty/profile_imgs/upload.php", imgAgence, agence.getNom_agence(), "amira") ;
            System.out.println("Response : "+response2);
            if(response2.equals("true")){
                System.out.println("telechargement complet");
            }else{
                System.out.println("pas de telechargement");
            }
            agence.setPhoto_agence("http://localhost/karhabty/profile_imgs/uploads/" + agence.getNom_agence());
            boolean res2 = DaoAgence.getInstance().AjouterAgence(agence);
            if(res2){
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Success");
                alert.setHeaderText("Success");
                alert.setContentText("Tout est bien");
                alert.showAndWait();
                agenceIsSet = null;
                FXMLAjouterAgenceLocation2Controller.userIsSet = null;
            }else{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error");
                alert.setContentText("il y a une erreur 2");
                alert.showAndWait();
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error");
            alert.setContentText("il y a une erreur");
            alert.showAndWait();
        }
    }

    @FXML
    private void uploadPiece(ActionEvent event) throws MalformedURLException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        FileChooser.ExtensionFilter ext1 = new FileChooser.ExtensionFilter("JPG files(*.jpg)","*.JPG");
        FileChooser.ExtensionFilter ext2 = new FileChooser.ExtensionFilter("PNG files(*.png)","*.PNG");
        fileChooser.getExtensionFilters().addAll(ext1,ext2);
        filePiece = fileChooser.showOpenDialog(img_agence.getScene().getWindow());
        if(filePiece != null){
            URL url = filePiece.toURI().toURL();
            img_piece.setImage(new Image(url.toExternalForm()));
        }
    }
    
}
