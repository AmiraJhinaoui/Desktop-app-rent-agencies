/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Agence;
import entity.User;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import utils.Pages;

/**
 * FXML Controller class
 *
 * @author 3D-Artist
 */
public class FXMLAjouterAgenceLocation1Controller implements Initializable {

    @FXML
    private Hyperlink InformationAgenceLocation;
    @FXML
    private Pane PaneInfo;
    @FXML
    private Hyperlink information_personnelles;
    
    
    
    
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
       
        
        try {
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/FXMLAjouterAgenceLocation2.fxml"));
            PaneInfo.getChildren().setAll(pane);
        } catch (IOException ex) {
            Logger.getLogger(FXMLAjouterAgenceLocation1Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }    

    @FXML
    private void loadInfosPerso(MouseEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/FXMLAjouterAgenceLocation2.fxml"));
        PaneInfo.getChildren().setAll(pane);
    }

    @FXML
    private void loadInfosAgence() throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/FXMLAjouterAgenceLocation3.fxml"));
        PaneInfo.getChildren().setAll(pane);
        System.out.println(FXMLAjouterAgenceLocation2Controller.userIsSet);
    }

    @FXML
    private void back(MouseEvent event) {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("/gui/FXMLCHOIXTYPEAGENCESIGNUP.fxml"));
            Stage myWindow = (Stage) PaneInfo.getScene().getWindow();
            Scene sc = new Scene(root);
            myWindow.setScene(sc);
            myWindow.setTitle(Pages.LOGIN);
            //myWindow.setFullScreen(true);
            myWindow.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
