/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DaoLocation;
import entity.Location;
import java.io.IOException;
import java.net.URL;
import java.time.ZoneId;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import utils.Session;


public class FXMLReserverLocationController implements Initializable {

    @FXML
    private DatePicker picker_fin;
    @FXML
    private DatePicker picker_debut;
    @FXML
    private Label label_prix;
    @FXML
    private Label label_agence;
    @FXML
    private CheckBox check_chauffeur;
    @FXML
    private Label laber_error;
    @FXML
    private Label label_gps;
    @FXML
    private Label label_clim;
    @FXML
    private Label label_airbag;
    @FXML
    private Label label_alarme;
    @FXML
    private Label label_marque;
    @FXML
    private Label label_carburant;
    @FXML
    private Label label_carrousserie;
    @FXML
    private Label label_boite;
    @FXML
    private Label label_nbr_porte;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        label_agence.setText(FXMLEspaceClientRechercheVoitureController.voiture.getAgence().getNom_agence());
        label_prix.setText(Float.toString(FXMLEspaceClientRechercheVoitureController.voiture.getPrixLocation()) + " DT");
        if(FXMLEspaceClientRechercheVoitureController.voiture.getGps()){
            label_gps.setText("oui");
        }else{
            label_gps.setText("non");
        }
        if(FXMLEspaceClientRechercheVoitureController.voiture.getClimatisation()){
            label_clim.setText("oui");
        }else{
            label_clim.setText("non");
        }
        if(FXMLEspaceClientRechercheVoitureController.voiture.getAirbag()){
            label_airbag.setText("oui");
        }else{
            label_airbag.setText("non");
        }
        if(FXMLEspaceClientRechercheVoitureController.voiture.getAlarme()){
            label_alarme.setText("oui");
        }else{
            label_alarme.setText("non");
        }
        label_marque.setText(FXMLEspaceClientRechercheVoitureController.voiture.getMarque());
        label_carburant.setText(FXMLEspaceClientRechercheVoitureController.voiture.getCarburant());
        label_carrousserie.setText(FXMLEspaceClientRechercheVoitureController.voiture.getCarrousserie());
        label_boite.setText(FXMLEspaceClientRechercheVoitureController.voiture.getBoite());
        label_nbr_porte.setText(Integer.toString(FXMLEspaceClientRechercheVoitureController.voiture.getNbr_porte()));
        
    }    

    @FXML
    private void louerVoiture(ActionEvent event) {
        if(picker_debut.getValue() == null || picker_fin.getValue() == null){
            laber_error.setText("entrez une date");
        }else{
            laber_error.setText("");
            
            java.util.Date dateDebut = java.util.Date.from(picker_debut.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
            java.sql.Date sqlDateDebut = new java.sql.Date(dateDebut.getTime());
            
            java.util.Date dateFin = java.util.Date.from(picker_fin.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
            java.sql.Date sqlDateFin = new java.sql.Date(dateFin.getTime());
            
            int compareDates = sqlDateDebut.compareTo(sqlDateFin);
            if(compareDates == 1 || compareDates == 0){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error");
                alert.setContentText("Date fin doit etre superieur a date debut");
                alert.showAndWait();
            }else{
                Location location = new Location(Session.getLoggedInUser(), FXMLEspaceClientRechercheVoitureController.voiture.getAgence(), FXMLEspaceClientRechercheVoitureController.voiture, sqlDateDebut, sqlDateFin, FXMLEspaceClientRechercheVoitureController.voiture.getPrixLocation(), check_chauffeur.isSelected(), false);
                if(DaoLocation.getInstance().ajouterLocation(location)){
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Success");
                    alert.setHeaderText("Success");
                    alert.setContentText("Ajout avec succée, attendre la confirmation du manager");
                    alert.showAndWait();
                }else{
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error");
                    alert.setContentText("Error somewhere");
                    alert.showAndWait();
                }
            }
        }
    }

    @FXML
    private void back(MouseEvent event) {
        Parent root;
             try {
                 root = FXMLLoader.load(getClass().getResource("/gui/FXMLEspaceClientRechercheVoiture.fxml"));
                 Stage myWindow = (Stage) label_agence.getScene().getWindow();
                 Scene sc = new Scene(root);
                 myWindow.setScene(sc);
                 myWindow.setTitle("page name");
                 myWindow.show();
             } catch (IOException ex) {
                 Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
             }
    }
    
}
