/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static controller.FXMLAjouterAgenceLocation3Controller.imgAgence;
import dao.DaoUser;
import entity.User;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.ZoneId;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.util.converter.IntegerStringConverter;
import utils.FileUpload;

/**
 * FXML Controller class
 *
 * @author 3D-Artist
 */
public class FXMLAjouterAgenceLocation2Controller implements Initializable {

    @FXML
    private ImageView image_user;
    
    File file = null;
    @FXML
    private TextField input_cin;
    @FXML
    private TextField input_nom;
    @FXML
    private TextField input_prenom;
    @FXML
    private TextField input_tel;
    @FXML
    private TextField input_mail;
    @FXML
    private PasswordField input_password;
    @FXML
    private TextField input_adr;
    @FXML
    private DatePicker input_date;
    @FXML
    private Label error_label;
    
    public static User userIsSet = null;
    public static File imgUser = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //FILTER TEXTFIELD TO ENTER ONLY NUMBERS
        UnaryOperator<TextFormatter.Change> integerFilter = change -> {
            String newText = change.getControlNewText();
            if (newText.matches("-?([1-9][0-9]*)?")) { 
                return change;
            }
            return null;
        };
        input_cin.setTextFormatter(new TextFormatter<Integer>(new IntegerStringConverter(), 0, integerFilter));
        input_tel.setTextFormatter(new TextFormatter<Integer>(new IntegerStringConverter(), 0, integerFilter));
        
        
        if(userIsSet != null){
            input_nom.setText(userIsSet.getNom());
            input_prenom.setText(userIsSet.getPrenom());
            input_cin.setText(Integer.toString(userIsSet.getId()));
            input_date.setValue(userIsSet.getDate_naissance().toLocalDate());
            input_tel.setText(Integer.toString(userIsSet.getTelephone()));
            input_mail.setText(userIsSet.getMail());
            input_adr.setText(userIsSet.getAdresse());
            if(imgUser != null){
                try {
                    URL urlImageUser = imgUser.toURI().toURL();
                    image_user.setImage(new Image(urlImageUser.toExternalForm()));
                    file = imgUser;
                } catch (MalformedURLException ex) {
                    Logger.getLogger(FXMLAjouterAgenceLocation2Controller.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }    

    @FXML
    private void uploadImage(ActionEvent event) throws MalformedURLException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        FileChooser.ExtensionFilter ext1 = new FileChooser.ExtensionFilter("JPG files(*.jpg)","*.JPG");
        FileChooser.ExtensionFilter ext2 = new FileChooser.ExtensionFilter("PNG files(*.png)","*.PNG");
        fileChooser.getExtensionFilters().addAll(ext1,ext2);
        file = fileChooser.showOpenDialog(image_user.getScene().getWindow());
        if(file != null){
            URL url = file.toURI().toURL();
            image_user.setImage(new Image(url.toExternalForm()));
        }
    }

    @FXML
    private void ajouterManager(ActionEvent event) {
        if(input_cin.getText().equals("") || input_nom.getText().equals("") || input_prenom.getText().equals("") || input_date.getValue().equals("") || input_tel.getText().equals("") || input_mail.getText().equals("") || input_password.getText().equals("") || input_adr.getText().equals("") || input_cin.getText().equals("0") || input_tel.getText().equals("0")){
            error_label.setText("Fill in the blanks idiot");
        }else{
            error_label.setText("");
            if(DaoUser.getInstance().checkCinExist(Integer.parseInt(input_cin.getText()))){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error");
                alert.setContentText("CIN existe deja !");
                alert.showAndWait();
            }else{
                User user = new User();
                user.setId(Integer.parseInt(input_cin.getText()));
                user.setNom(input_nom.getText());
                user.setPrenom(input_prenom.getText());
                user.setMail(input_mail.getText());
                user.setTelephone(Integer.parseInt(input_tel.getText()));
                user.setPassword(input_password.getText());
                user.setAdresse(input_adr.getText());
                java.util.Date date = java.util.Date.from(input_date.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
                java.sql.Date sqlDate = new java.sql.Date(date.getTime());
                user.setDate_naissance(sqlDate);
                user.setApprouved(false);
                user.setBanned(false);
                if(file == null){
                    user.setImage("default");
                }else{
                    // UPLOAD THE IMAGE HERE
                    /*FileUpload fileUpload = new FileUpload () ;
                    System.out.println(file.getAbsoluteFile());
                    String response = fileUpload.executeMultiPartRequest("http://localhost/karhabty/profile_imgs/upload.php", file, input_prenom.getText(), "amira") ;
                    System.out.println("Response : "+response);
                    if(response.equals("true")){
                        System.out.println("upload complete");
                    }else{
                        System.out.println("no upload");
                    }
                    user.setImage("http://localhost/karhabty/profile_imgs/uploads/" + input_prenom.getText());*/
                    user.setImage("from_file");
                }
                user.setRole("ROLE_MANAGER_LOCATION");
                userIsSet = user;
                imgUser = file;
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Success");
                alert.setHeaderText("Success");
                alert.setContentText("well done");
                alert.showAndWait();
                /*boolean res = DaoUser.getInstance().registerClient(user);
                System.out.println(res); 
                if(res){
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Success");
                    alert.setHeaderText("Success");
                    alert.setContentText("Infos ajouté avec succée, completer...");
                    alert.showAndWait();
                    
                    
                }else{
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error");
                    alert.setContentText("Erreur lors de l'ajout");
                    alert.showAndWait();
                }*/
            }
        }
        
    }
    
}
